import React from 'react'
import PropTypes from 'prop-types'
import { Link, graphql } from 'gatsby'
import Layout from '../components/Layout'
import netlifyIdentity from 'netlify-identity-widget'
netlifyIdentity.init()
export default class IndexPage extends React.Component {
  render() {
    const { data } = this.props
    const { edges: posts } = data.allMarkdownRemark

    return (
      <Layout>
        <section class="hero is-small is-dark">
          <div class="hero-body">
            <div class="container">
              <h1 class="title">
                <i class="fal fa-newspaper hero-icon" /> News &amp; Updates
              </h1>
              <h2 class="subtitle">
                Stay up to date with the development of Samurai Zero
              </h2>
            </div>
          </div>
        </section>
        <section className="section home">
          <div className="container">
            {posts.map(({ node: post }) => (
              <div
                className="content"
                style={{
                  border: '1px solid #0a0d14',
                  background: '#0a0d14',
                  padding: '2em 4em',
                }}
                key={post.id}
              >
                <p>
                  <Link className=" is-4 news-title" to={post.fields.slug}>
                    {post.frontmatter.title}
                  </Link>
                </p>
                <p>
                  {post.excerpt}
                  <br />
                  <br />
                  <Link
                    className="button is-outlined news-btn"
                    to={post.fields.slug}
                  >
                    Keep Reading &emsp;&emsp; <i class="fal fa-caret-right" />
                  </Link>
                </p>
              </div>
            ))}
          </div>
        </section>
      </Layout>
    )
  }
}

IndexPage.propTypes = {
  data: PropTypes.shape({
    allMarkdownRemark: PropTypes.shape({
      edges: PropTypes.array,
    }),
  }),
}

export const pageQuery = graphql`
  query IndexQuery {
    allMarkdownRemark(
      sort: { order: DESC, fields: [frontmatter___date] }
      filter: { frontmatter: { templateKey: { eq: "blog-post" } } }
    ) {
      edges {
        node {
          excerpt(pruneLength: 400)
          id
          fields {
            slug
          }
          frontmatter {
            title
            templateKey
            date(formatString: "MMMM DD, YYYY")
          }
        }
      }
    }
  }
`
